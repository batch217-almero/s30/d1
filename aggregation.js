
// AGGREGATION

// Insert items to Database
db.fruits.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id: 1,
		onSale: true,
		origin: ["Philippines", "US"]
	},
	{
		name: "Banna",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id: 2,
		onSale: true,
		origin: ["Philippines", "Ecuador"]
	},
	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id: 1,
		onSale: true,
		origin: ["US", "China"]
	},
	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id: 2,
		onSale: false,
		origin: ["Philippines", "India"]
	}
]);

// Used to generate manipulated dataand perform operation to create filtered results used for analyzing data

// Use AGGREGATE Method
db.fruits.aggregate([

	{$match: {onSale: true}},//filter using match(items na naka sale lang)

	{$group: {
		_id: "$supplier_id", //group by ID
		total: { $sum: "$stock"} //get total sum of stocks per ID
	}}
]);

// FIELD PROJECTION WITH AGGREGATION
db.fruits.aggregate([
	{$match: {onSale: true}},//filter using match(items na naka sale lang)
	{$group: {_id: "$supplier_id", //group by ID
	total: {$sum: "$stock"}}}, //get total sum of stocks per ID
	{$project: {_id: 0}}//hide _id in output display
]);


// SORTING AGGREGATED RESULTS -change order of aggregated results
db.fruits.aggregate([
	{$match: {onSale: true}},//filter using match(items na naka sale lang)
	{$group: {_id: "$supplier_id", //group by ID
	total: {$sum: "$stock"}}}, //get total sum of stocks per ID
	{$sort: {total: -1}}//"-1" sort total descending, "1" is ascending 
]);


// DECONSTRUCT ARRAY 
db.fruits.aggregate([
	{$unwind: "$origin"}
]);

//DECONSTRUCT ARRAY + GROUPINGS
db.fruits.aggregate([
	{$unwind: "$origin"}, //deconstruct array [origin]
	{$group: {_id: "$origin", kinds: {$sum: 1}}} //group using origin and get sum (per fruit and country pairing)
]);


// AVERAGE ($avg)
db.fruits.aggregate ([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", yellow_fruits_stock: {$avg: "$stock"}}}
]);

// COUNT ($count)
db.fruits.aggregate ([
	{$match: {color: "Yellow"}},
	{$count: "Yellow Fruits"}
]);

// MIN and MAX ($count)
db.fruits.aggregate ([
	{$match: {color: "Yellow"}},
	{$group: {_id: "$color", yellow_fruits_stock: {$min: "$stock"}}}	
]);